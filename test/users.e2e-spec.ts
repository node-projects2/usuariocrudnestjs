import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { UsersModule } from '../src/users/users.module';
import { UserDTO } from '../src/users/user.dto';
import { TypeORMExceptionFilter } from '../src/filters/typeorm-exception.filter';

describe('UsersController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule, UsersModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalFilters(new TypeORMExceptionFilter());
    await app.init();
  });

  it('Users CRUD', async (callback) => {
    const server = request(app.getHttpServer());

    // Verificación del GET
    const currentGetAllRequest = await server.get('/users').expect(200);
    const currentSize = currentGetAllRequest.body.length;

    // Verificación del POST
    const newUser: UserDTO = {
      name: 'David'
    };

    // type puede ser form o application/json
    const newUserRequest = await server.post('/users').type('form')
      .send(newUser).expect(201);
    expect(newUserRequest.body.name).toBe(newUser.name);

    const postNewUserRequest = await server.get('/users').expect(200);
    const postNewSize = postNewUserRequest.body.length;
    expect(postNewSize).toBe(currentSize + 1);

    // Verificación del PUT
    const id = newUserRequest.body.id;
    const getUserByIdRequest = await server.get(`/users/${id}`).expect(200);
    expect(getUserByIdRequest.body.id).toBe(id);

    const updateUser: UserDTO = {
      id: newUserRequest.body.id,
      name: 'David Marchante'
    };

    const updateUserRequest = await server.put(`/users/${updateUser.id}`)
      .expect(200).type('form').send(updateUser);
    expect(updateUserRequest.body.name).toEqual(updateUser.name);

    // Verificar DELETE
    await server.delete(`/users/${updateUser.id}`).expect(200);
    const postDeleteGetAllRequest = await server.get('/users').expect(200);
    const postDeleteSize = postDeleteGetAllRequest.body.length;

    expect(postDeleteSize).toBe(currentSize);

    // para no tener problemas de memory lint y así sabe que ha acabado
    callback();
  });
});
