import { Controller, Get, Param, Post, Body, Put, Delete } from "@nestjs/common";
import { UserDTO } from "./user.dto";
import { UsersService } from "./users.service";

@Controller('users')
export class UsersController {

    constructor(
        private userService: UsersService
    ) { }

    users: UserDTO[] = []

    @Get()
    async getAllUsers(): Promise<UserDTO[]> {
        return await this.userService.getAllUsers();
    }

    @Get(':id')
    async getById(@Param('id') id: string): Promise<UserDTO> {
        return await this.userService.getUserById(id);
    }

    @Post()
    async newUser(@Body() user: UserDTO): Promise<UserDTO> {
        return await this.userService.newUser(user);
    }

    @Put(':id')
    async updateUser(@Param('id') id: string, @Body() user: UserDTO): Promise<UserDTO> {
        return await this.userService.updateUser(id, user);
    }

    @Delete(':id')
    async deleteUser(@Param('id') id: string): Promise<void> {
        return await this.userService.deleteUser(id);
    }

}
