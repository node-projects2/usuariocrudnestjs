import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { UserEntity } from "./user.entity";
import { Repository, DeleteResult } from "typeorm";
import { UsersMapper } from "./users.mapper";
import { UserDTO } from "./user.dto";

@Injectable()
export class UsersRepository {

    constructor(
        @InjectRepository(UserEntity) private usersRepository: Repository<UserEntity>,
        private userMapper: UsersMapper
    ) { }

    getAllUsers(): Promise<UserEntity[]> {
        return this.usersRepository.find();
    }

    getUserById(id: string): Promise<UserEntity> {
        return this.usersRepository.findOne(id);
    }

    newUser(userDTO: UserDTO): Promise<UserEntity> {
        const newUser = this.userMapper.dtoToEntity(userDTO);
        return this.usersRepository.save(newUser);
    }

    async updateUser(id: string, userDTO: UserDTO): Promise<UserEntity> {
        const updateUserDTO = new UserDTO(id, userDTO.name);
        const updateUser = this.userMapper.dtoToEntity(updateUserDTO);
        await this.usersRepository.update(id, updateUser);
        return this.usersRepository.findOne(id);
    }

    deleteUser(id: string): Promise<DeleteResult> {
        return this.usersRepository.delete(id);
    }
}
