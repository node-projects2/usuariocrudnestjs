import { Injectable } from "@nestjs/common";
import { UsersMapper } from "./users.mapper";
import { UsersRepository } from "./users.repository";
import { UserDTO } from "./user.dto";
import { UserEntity } from "./user.entity";

@Injectable()
export class UsersService {

    constructor(
        private usersRepository: UsersRepository,
        private usersMapper: UsersMapper
    ) { }

    async getAllUsers(): Promise<UserDTO[]> {
        const users: UserEntity[] = await this.usersRepository.getAllUsers();
        return users.map(user => this.usersMapper.entityToDTO(user));
    }

    async getUserById(id: string): Promise<UserDTO> {
        const user: UserEntity = await this.usersRepository.getUserById(id);
        return this.usersMapper.entityToDTO(user);
    }

    async newUser(userDTO: UserDTO): Promise<UserDTO> {
        const newUser: UserEntity = await this.usersRepository.newUser(userDTO);
        return this.usersMapper.entityToDTO(newUser);
    }

    async updateUser(id: string, userDTO: UserDTO): Promise<UserDTO> {
        const updateUser: UserEntity = await this.usersRepository.updateUser(id, userDTO);
        return this.usersMapper.entityToDTO(updateUser);
    }

    async deleteUser(id: string): Promise<void> {
        await this.usersRepository.deleteUser(id);
    }
}
